# Brazilian Portuguese translation of https://www.gnu.org/testimonials/testimonials.html
# Copyright (C) 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Fernando Lozano <fsl@centroin.com.br>, 2001.
# Rafael Fontenelle <rafaelff@gnome.org>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: testimonials.html\n"
"POT-Creation-Date: 2014-04-05 00:03+0000\n"
"PO-Revision-Date: 2018-04-01 17:01-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <www-pt-br-general@gnu.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: Content of: <title>
msgid "GNU Testimonials - GNU Project - Free Software Foundation"
msgstr "Testemunhos do GNU - Projeto GNU - Free Software Foundation"

#. type: Content of: <h2>
msgid "GNU Testimonials"
msgstr "Testemunhos do GNU"

#. type: Content of: <p>
msgid ""
"Free software isn't simply a pastime for programmers; it's a productive "
"contributor to people and businesses all over the world.  From ornamental "
"ribbon makers, to scientific researchers, free software aids hundreds of "
"thousands of people in their everyday work.  Its development continues at "
"blazing speed to fill the insatiable needs of users in all types of "
"applications."
msgstr ""
"O software livre não é apenas um passatempo para programadores; é um "
"contribuidor produtivo para pessoas e negócios por todo o mundo. Desde "
"fabricantes de ornamentos até pesquisadores científicos, o software livre "
"ajuda centenas de milhares de pessoas no seu trabalho diário. O seu "
"desenvolvimento prossegue em ritmo aceleradíssimo para preencher as "
"necessidades insaciáveis de usuários em todos os tipos de aplicativos."

#. type: Content of: <p>
msgid ""
"The GNU Project has been a major influence on free software development for "
"more than a decade, creating a myriad of important tools such as robust "
"compilers, powerful text editors, and even a fully functional operating "
"system.  The project was started in 1984 by MIT programmer Richard Stallman "
"to create a free, Unix-like operating environment.  Since then, thousands of "
"programmers have joined in the effort to produce free, high quality software "
"which is accessible to everyone."
msgstr ""
"O Projeto GNU tem sido uma grande influência no desenvolvimento de software "
"livre há mais de uma década, criando uma miríade de ferramentas importantes, "
"como compiladores robustos, poderosos editores de texto e até mesmo um "
"sistema operacional completamente funcional. O projeto foi iniciado em 1984 "
"pelo programador do MIT Richard Stallman para criar um ambiente operacional "
"livre semelhante ao Unix. Desde então, milhares de programadores se juntaram "
"ao esforço de produzir software livre e de alta qualidade que está acessível "
"para todas as pessoas."

#. type: Content of: <p>
msgid ""
"These testimonials challenge the misconceptions that free software is "
"impractical, unreliable, and unsupported."
msgstr ""
"Estes testemunhos desafiam as concepções errôneas de que o software livre é "
"antiprático, instável e não tem suporte."

#. type: Content of: <ul><li>
msgid "<a href=\"/testimonials/useful.html\">Free Software is Useful</a>"
msgstr "<a href=\"/testimonials/useful.html\">O Software Livre é útil</a>"

#. type: Content of: <ul><li>
msgid "<a href=\"/testimonials/reliable.html\">Free Software is Reliable</a>"
msgstr ""
"<a href=\"/testimonials/reliable.html\">O Software Livre é confiável</a>"

#. type: Content of: <ul><li>
msgid "<a href=\"/testimonials/supported.html\">Free Software is Supported</a>"
msgstr ""
"<a href=\"/testimonials/supported.html\">O Software Livre tem suporte</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"http://www.dwheeler.com/oss_fs_why.html\">Why Open Source "
"Software / Free Software (OSS/FS)? Look at the Numbers!</a>"
msgstr ""
"<a href=\"http://www.dwheeler.com/oss_fs_why.html\">Por que software de "
"código aberto / Software Livre (OSS/FS)? Veja os números!</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"http://www.dwheeler.com/essays/high-assurance-floss.html\">High "
"Assurance (for Security or Safety) and Free-Libre / Open Source Software "
"(FLOSS)... with Lots on Formal Methods</a>"
msgstr ""
"<a href=\"http://www.dwheeler.com/essays/high-assurance-floss.html\">Alta "
"garantia (para segurança ou proteção) e Software Livre / Código aberto "
"(FLOSS)... com muitos métodos formais</a>"

#. type: Content of: <p>
msgid "Other testimonials:"
msgstr "Outros testemunhos:"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"testimonial_mondrup.html\">A database application for organ "
"transplantation waiting lists and quality control</a>"
msgstr ""
"<a href=\"testimonial_mondrup.html\">Um aplicativo de banco de dados para "
"listas de espera de transplante de órgãos e controle de qualidade</a>"

#. type: Content of: <ul><li>
msgid "<a href=\"testimonial_cadcam.html\">CAD/CAM Software</a>"
msgstr "<a href=\"testimonial_cadcam.html\">Software para CAD/CAM</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"testimonial_HIRLAM.html\">HIRLAM Consortium</a> (meteorological "
"research)"
msgstr ""
"<a href=\"testimonial_HIRLAM.html\">HIRLAM Consortium</a> (pesquisa "
"meteorológica)"

#. type: Content of: <ul><li>
msgid "<a href=\"testimonial_media.html\">Publishing and music production</a>"
msgstr "<a href=\"testimonial_media.html\">Publicação e produção de música</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"testimonial_research_ships.html\">Research ships</a> performing "
"deep seismic acquisition"
msgstr ""
"<a href=\"testimonial_research_ships.html\">Navios de pesquisa</a> realizam "
"aquisição sísmica profunda"

#. type: Content of: <p>
msgid ""
"If you have a testimonial to make about free software, we'd like hear about "
"it and add it to this page.  Longer, detailed testimonials are much more "
"useful than short testimonials.  You can email your testimonials to <a href="
"\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>."
msgstr ""
"Se você tem um testemunho que gostaria de fazer sobre o software livre, nós "
"gostaríamos de ouvi-lo e de adicioná-lo a esta página. Testemunhos longos, "
"detalhados, são bem mais úteis do que testemunhos curtos. Você pode enviar "
"os seus testemunhos para<a href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</"
"a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/\">outros "
"meios de contatar</a> a FSF. Links quebrados e outras correções ou sugestões "
"podem ser enviadas para <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"A equipe de traduções para o português brasileiro se esforça para oferecer "
"traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por "
"favor, envie seus comentários e sugestões em geral sobre as traduções para "
"<a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte o <a href=\"/server/standards/README.translations.html"
"\">Guia para as traduções</a> para mais informações sobre a coordenação e o "
"envio de traduções das páginas deste site."

# | Copyright &copy; [-2014, 2015, 2016, 2017-] {+1998, 1999, 2000, 2001,
# | 2002, 2006, 2007, 2012, 2013, 2014+} Free Software Foundation, Inc.
#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 1998, 1999, 2000, 2001, 2002, 2006, 2007, 2012, 2013, 2014 "
"Free Software Foundation, Inc."
msgstr ""
"Copyright &copy; 1998, 1999, 2000, 2001, 2002, 2006, 2007, 2012, 2013, 2014 "
"Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/3.0/us/\">Creative Commons Attribution-"
"NoDerivs 3.0 United States License</a>."
msgstr ""
"Esta página está licenciada sob uma licença <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/3.0/us/deed.pt_BR\">Creative Commons "
"Atribuição-SemDerivações 3.0 Estados Unidos</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduzido por: Fernando Lozano <a href=\"mailto:fsl@centroin.com.br\">&lt;"
"fsl@centroin.com.br&gt;</a>, 2001. Rafael Fontenelle <a href=\"mailto:"
"rafaelff@gnome.org\">&lt;rafaelff@gnome.org&gt;</a>, 2017"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última atualização:"
