# Brazilian Portuguese translation of https://www.gnu.org/licenses/old-licenses/fdl-1.2.html
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Rafael Fontenelle <rafaelff@gnome.org>, 2018-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: fdl-1.2.html\n"
"POT-Creation-Date: 2020-12-08 11:55+0000\n"
"PO-Revision-Date: 2020-12-13 17:38-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <www-pt-br-general@gnu.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#. type: Content of: <title>
msgid ""
"GNU Free Documentation License 1.2 - GNU Project - Free Software Foundation"
msgstr ""
"Licença de Documentação Livre GNU 1.2 - Projeto GNU - Free Software "
"Foundation"

#. type: Content of: <h2>
msgid "GNU Free Documentation License 1.2"
msgstr "Licença de Documentação Livre GNU, versão 1.2"

#. type: Content of: <h3>
msgid "Related Pages"
msgstr "Páginas relacionadas"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/licenses/fdl.html\">The latest version of the FDL, version 1.3</a>"
msgstr "<a href=\"/licenses/fdl.html\">A última versão da FDL, versão 1.3</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/philosophy/free-doc.html\">Free software and free documentation</"
"a>"
msgstr ""
"<a href=\"/philosophy/free-doc.html\">Software livre e documentação livre</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/licenses/gpl-violation.html\">What to do if you see a possible "
"GFDL violation</a>"
msgstr ""
"<a href=\"/licenses/gpl-violation.html\">O que fazer se você encontrar uma "
"possível violação da GFDL</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/licenses/why-gfdl.html\">Why publishers should use the GNU Free "
"Documentation License</a>"
msgstr ""
"<a href=\"/licenses/why-gfdl.html\">Por que editores devem usar a Licença de "
"Documentação Livre GNU</a>"

#. type: Content of: <ul><li>
msgid "<a href=\"/licenses/fdl-howto.html\">Tips on how to use the FDL</a>"
msgstr "<a href=\"/licenses/fdl-howto.html\">Dicas sobre como usar a FDL</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/licenses/fdl-howto-opt.html\">Using the optional features of the "
"GFDL</a>"
msgstr ""
"<a href=\"/licenses/fdl-howto-opt.html\">Usando os recursos opcionais da "
"GFDL</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/licenses/old-licenses/fdl-1.2-translations.html\">Translations of "
"GFDLv1.2</a>"
msgstr ""
"<a href=\"/licenses/old-licenses/fdl-1.2-translations.html\">Traduções da "
"GFDLv1.2</a>"

#. type: Content of: <ul><li>
msgid ""
"The GNU Free Documentation License version 1.2 (GFDLv1.2) in other formats: "
"<a href=\"/licenses/old-licenses/fdl-1.2.txt\">plain text</a>, <a href=\"/"
"licenses/old-licenses/fdl-1.2.texi\">Texinfo</a>, <a href=\"/licenses/old-"
"licenses/fdl-1.2.tex\">LaTeX</a>, <a href=\"/licenses/old-licenses/fdl-1.2-"
"standalone.html\">standalone HTML</a>, <a href=\"/licenses/old-licenses/"
"fdl-1.2.xml\">Docbook</a>, <a href=\"/licenses/old-licenses/fdl-1.2.md"
"\">Markdown</a>, <a href=\"/licenses/old-licenses/fdl-1.2.odt\">ODF</a>, <a "
"href=\"/licenses/old-licenses/fdl-1.2.rtf\">RTF</a>"
msgstr ""
"A Licença da Documentação Livre GNU versão 1.2 (GFDLv1.2) em outros "
"formatos: <a href=\"/licenses/old-licenses/fdl-1.2.txt\">texto simples</a> "
"<a href=\"/licenses/old-licenses/fdl-1.2.texi\">Texinfo</a>, <a href=\"/"
"licenses/old-licenses/fdl-1.2.tex\">LaTeX</a>, <a href=\"/licenses/old-"
"licenses/fdl-1.2-standalone.html\">HTML autônomo</a>, <a href=\"/licenses/"
"old-licenses/fdl-1.2.xml\">Docbook</a>, <a href=\"/licenses/old-licenses/"
"fdl-1.2.md\">Markdown</a>, <a href=\"/licenses/old-licenses/fdl-1.2.odt"
"\">ODF</a>, <a href=\"/licenses/old-licenses/fdl-1.2.rtf\">RTF</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/licenses/old-licenses/old-licenses.html#FDL\">Old versions of the "
"GFDL</a>"
msgstr ""
"<a href=\"/licenses/old-licenses/old-licenses.html#FDL\">Versões antigas da "
"GFDL</a>"

#. type: Content of: <p>
msgid ""
"diff files showing the changes between versions 1.1 and 1.2 of the GNU Free "
"Documentation License are available:"
msgstr ""
"Arquivos diff mostrando as alterações entre as versões 1.1 e 1.2 da Licença "
"de Documentação Livre estão disponíveis:"

#. type: Content of: <ul><li>
msgid "<a href=\"/licenses/fdl-1.2-diff.txt\">Plain context diff</a>"
msgstr "<a href=\"/licenses/fdl-1.2-diff.txt\">Diff de contexto simples</a>"

#. type: Content of: <ul><li>
msgid "<a href=\"/licenses/fdl-1.2-wdiff.txt\">Word diff</a>"
msgstr "<a href=\"/licenses/fdl-1.2-wdiff.txt\">Diff por palavra</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/licenses/fdl-1.2-pdiff.ps\">Word diff in PostScript format</a>"
msgstr ""
"<a href=\"/licenses/fdl-1.2-pdiff.ps\">Diff por palavra no formato "
"PostScript</a>"

#. type: Content of: <h3>
msgid "Table of Contents"
msgstr "Conteúdo"

#. type: Content of: <ul><li>
msgid ""
"<a id=\"TOC1\" href=\"#SEC1\">Current version of the GNU Free Documentation "
"License</a>"
msgstr ""
"<a id=\"TOC1\" href=\"#SEC1\">Versão atual da Licença de Documentação Livre "
"GNU</a>"

#. type: Content of: <ul><li>
msgid ""
"<a id=\"TOC4\" href=\"#SEC4\">How to use this License for your documents</a>"
msgstr ""
"<a id=\"TOC4\" href=\"#SEC4\">Como usar essa Licença para seus documentos</a>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/\">outros "
"meios de contatar</a> a FSF. Links quebrados e outras correções ou sugestões "
"podem ser enviadas para <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"A equipe de traduções para o português brasileiro se esforça para oferecer "
"traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por "
"favor, envie seus comentários e sugestões em geral sobre as traduções para "
"<a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte o <a href=\"/server/standards/README.translations.html"
"\">Guia para as traduções</a> para mais informações sobre a coordenação e o "
"envio de traduções das páginas deste site."

#. type: Content of: <div><p>
msgid "Copyright notice above."
msgstr "Aviso de direitos autorais acima."

#. type: Content of: <div><p>
msgid ""
"Verbatim copying and distribution of this entire article is permitted in any "
"medium without royalty provided this notice is preserved."
msgstr ""
"Cópia literal e distribuição deste artigo é permitida em qualquer meio sem "
"royalties, desde que esse aviso seja preservado."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduzido por: Rafael Fontenelle <a href=\"mailto:rafaelff@gnome.org\">&lt;"
"rafaelff@gnome.org&gt;</a>, 2018-2020."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última atualização:"
